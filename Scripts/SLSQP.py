from scipy.optimize import minimize, LinearConstraint
import numpy as np
import json
import sys
from timeit import timeit, default_timer

file_path = sys.argv[1]
f = open(file_path)
data = json.load(f)
start_time = default_timer()
factor = float(1e+09)
c = np.array(data["c"])
A_ub = np.array(data["A_ub"])
A_eq = np.array(data["A_eq"])
b_ub = np.array(data["b_ub"])
b_eq = np.array(data["b_eq"])

def fun(x):
    return c.dot(x)/factor

def eq_cons_i(x,i):
    return -A_eq[i].dot(x) + b_eq[i]

def ineq_cons_i(x,i):
    return -A_ub[i].dot(x) + b_ub[i]

def print_results(res):
    try:
        print("=======================================")
        print(f"Vector óptimo: {res.x}")
        print(f"Status: {res.message}")
        print(f"Valor óptimo de la función objetivo: {factor*res.fun}")
        print(f"Success: {res.success}")
        print(f"Numero de iteraciones: {res.nit}")
        print("=======================================")

        dictionary = {
        "Status": res.message,
        "Vector_optimo": list(res.x),
        "Valor_optimo": factor*res.fun,
        "success": f"{res.success}",
        "num_it": res.nit,
        "time_taken": f"{default_timer() - start_time : .4f}"
        }
    except:
        print("=======================================")
        print(f"Vector óptimo: {res}")
        print(f"Valor óptimo de la función objetivo: {factor*fun(res)}")
        print("=======================================")
        dictionary = {
        "Vector_optimo": list(res),
        "Valor_optimo": factor*fun(res),
        "time_taken": f"{default_timer() - start_time : .4f}"
        }
    json_object = json.dumps(dictionary)

    # Writing to sample.json
    with open(file_path.replace('.json','.ans_slsqp'), "w") as outfile:
        outfile.write(json_object)

def check_time(res):
    if default_timer() - start_time > 3600:
        print_results(res)
        raise Exception("Se ha sobrepasado el tiempo limite para este problema (1 hora)")


def run():
    cons = [] # Restricciones del problema

    cons += [{'type': 'eq', 'fun': eq_cons_i, 'args': (i,)} for i in np.arange(len(A_eq))]
    cons += [{'type': 'ineq', 'fun': ineq_cons_i, 'args': (i,)} for i in np.arange(len(A_ub))]

    res = minimize(fun, x0 = [0]*len(c), constraints = cons, method = 'SLSQP', options= {'maxiter': 9999999}, bounds = [(0.0,None)]*len(c), callback = check_time)
    return res

if __name__ == '__main__':
    res = run()
    print_results(res)
    print(f"Tiempo total de ejecución:  {default_timer() - start_time : .4f}s")
