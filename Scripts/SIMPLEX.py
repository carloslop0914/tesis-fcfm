from scipy.optimize import linprog
import json
from math import inf
from timeit import timeit, default_timer
import numpy as np
import sys

file_path = sys.argv[1]
f = open(file_path)
data = json.load(f)
start_time = default_timer()

def print_results(res):
    print("=================================================")
    print(f"Status: {res.message}")
    print(f"Vector óptimo: {res.x}")
    print(f"Valor óptimo de la función objetivo: {res.fun}")
    print(f"Success: {res.success}")
    print(f"Numero de iteraciones: {res.nit}")
    print("=================================================")
    # Serializing json
    dictionary = {
        "Status": res.message,
        "Vector_optimo": list(res.x),
        "Valor_optimo": res.fun,
        "success": res.success,
        "num_it": res.nit,
        "time_taken": f"{default_timer() - start_time : .4f}"
    }
    json_object = json.dumps(dictionary)

    # Writing to sample.json
    with open(file_path.replace('.json','.ans'), "w") as outfile:
        outfile.write(json_object)

def check_time(res):
    if default_timer() - start_time > 3600:
        print_results(res)
        raise Exception("Se ha sobrepasado el tiempo limite para este problema (1 hora)")

def run():
    res = linprog(
        np.array(data["c"]),
        A_ub = np.matrix(data["A_ub"]),
        b_ub = np.array(data["b_ub"]),
        A_eq = np.matrix(data["A_eq"]),
        b_eq = np.array(data["b_eq"]),
        method = 'simplex',
        callback = check_time,
        options = {'presolve': True, 'autoscale': True, 'maxiter': 9999999}
        )
    return res

if __name__ == '__main__':
    res = run()
    print_results(res)
    print(f"Tiempo total de ejecución:  {default_timer() - start_time : .4f}s")
