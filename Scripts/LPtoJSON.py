import json
import numpy as np
import sys

file_path = sys.argv[1]
print(file_path)

lp = open(file_path)

var_idx = {}

def getCoeficients(line, number_of_variables, negative = False):
    x = line.split()
    ans = {}
    coef = None
    sign = -1 if negative else 1
    for elem in x:
        if elem == '+':
            continue
        elif elem == '-':
            sign *= -1
            continue
        else:
            try:
                coef = sign*float(elem)
                sign = -1 if negative else 1
            except:
                var = elem
                if var not in var_idx.keys():
                    var_idx[var] = number_of_variables
                    number_of_variables += 1
                ans[var_idx[var]] = coef or 1.0*sign
                coef = None
    return ans

lp.readline() #Ignorar el comentario de la primera linea
obj = lp.readline() #Minimize/Maximize

fun = lp.readline().split(':')[1].strip() #Funcion objetivo
fun_c = getCoeficients(fun, 0)
number_of_variables = len(var_idx)

_ = lp.readline() #Subject to

current_line = lp.readline().split(':')[1].strip()
eq_constraints = []
uneq_constraints = []
while current_line != "End\n":
    if not ('<' in current_line or '>' in current_line) and current_line != "Bounds\n":
        eq_constraints.append(current_line)
    elif current_line != "Bounds\n":
        uneq_constraints.append(current_line)

    current_line = lp.readline()
    if ':' in current_line:
        current_line = current_line.split(':')[1].strip()

b_eq = []
eq_coefs = []

for line in eq_constraints:
    eq, val = line.split('=')
    b_eq.append(float(val))
    eq_coefs.append(getCoeficients(eq, number_of_variables))
    number_of_variables = len(var_idx)

b_lt = []
lt_coefs = []


for line in uneq_constraints:
    if "<" in line:
        eq, val = line.split('<=')
        b_lt.append(float(val))
        lt_coefs.append(getCoeficients(eq, number_of_variables))
    else:
        eq, val = line.split('>=')
        b_lt.append(-float(val))
        lt_coefs.append(getCoeficients(eq, number_of_variables, True))
    number_of_variables = len(var_idx)

A_eq = []
A_lt = []

for dicts in eq_coefs:
    aux = [0.0]*number_of_variables
    for key in dicts.keys():
        aux[key] = dicts[key]
    A_eq.append(aux)

for dicts in lt_coefs:
    aux = [0.0]*number_of_variables
    for key in dicts.keys():
        aux[key] = dicts[key]
    A_lt.append(aux)

c = [0.0]*number_of_variables
for key in fun_c.keys():
    c[key] = fun_c[key]

print(len(c))
print(len(A_eq), len(A_eq[0]))
print(len(b_eq))
print(len(A_lt), len(A_lt[0]))
print(len(b_lt))
print(f"Total Variables: {number_of_variables}")

# Data to be written
dictionary ={
    "A_ub": A_lt,
    "A_eq": A_eq,
    "b_ub": b_lt,
    "b_eq": b_eq,
    "c": c
}

# Serializing json
json_object = json.dumps(dictionary)

# Writing to sample.json
with open(file_path.replace('.lp','.json'), "w") as outfile:
    outfile.write(json_object)
